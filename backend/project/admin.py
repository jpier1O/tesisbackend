from django.contrib import admin

# Register your models here.
from .models import TipoCliente, Servicio, Tecnologia, Cliente, Rol, Recurso, Perfil, Usuario

admin.site.register(TipoCliente)
admin.site.register(Servicio)
admin.site.register(Tecnologia)
admin.site.register(Cliente)
admin.site.register(Rol)
admin.site.register(Recurso)
admin.site.register(Perfil)
admin.site.register(Usuario)
