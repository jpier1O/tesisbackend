
# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import generics
from . import models
from . import serializer

from rest_framework import status, viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

class TipoClienteList(generics.ListCreateAPIView):
    queryset = models.TipoCliente.objects.all()
    serializer_class = serializer.TipoClienteSerializer

class TipoClienteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.TipoCliente.objects.all()
    serializer_class = serializer.TipoClienteSerializer



class ServicioList(generics.ListCreateAPIView):
    queryset = models.Servicio.objects.all()
    serializer_class = serializer.ServicioSerializer

class ServicioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Servicio.objects.all()
    serializer_class = serializer.ServicioSerializer


class ClienteList(generics.ListCreateAPIView):
    queryset = models.Cliente.objects.all()
    serializer_class = serializer.ClienteSerializer

class ClienteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Cliente.objects.all()
    serializer_class = serializer.ClienteSerializer

class TecnologiaList(generics.ListCreateAPIView):
    queryset = models.Tecnologia.objects.all()
    serializer_class = serializer.TecnologiaSerializer

class TecnologiaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Tecnologia.objects.all()
    serializer_class = serializer.TecnologiaSerializer

class RecursoList(generics.ListCreateAPIView):
    queryset = models.Recurso.objects.all()
    serializer_class = serializer.RecursoSerializer

class RecursoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Recurso.objects.all()
    serializer_class = serializer.RecursoSerializer

class RolList(generics.ListCreateAPIView):
    queryset = models.Rol.objects.all()
    serializer_class = serializer.RolSerializer

class RolDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Rol.objects.all()
    serializer_class = serializer.RolSerializer


class UsuarioList(generics.ListCreateAPIView):
    queryset = models.Usuario.objects.all()
    serializer_class = serializer.UsuarioSerializer

class UsuarioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Usuario.objects.all()
    serializer_class = serializer.UsuarioSerializer


class PerfilList(generics.ListCreateAPIView):
    queryset = models.Perfil.objects.all()
    serializer_class = serializer.PerfilSerializer

class PerfilDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Perfil.objects.all()
    serializer_class = serializer.PerfilSerializer

class ProyectoList(generics.ListCreateAPIView):
    queryset = models.Proyecto.objects.all()
    serializer_class = serializer.ProyectoSerializer

class ProyectoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Proyecto.objects.all()
    serializer_class = serializer.ProyectoSerializer

class CotizacionList(generics.ListCreateAPIView):
    queryset = models.Cotizacion.objects.all()
    serializer_class = serializer.CotizacionSerializer

class CotizacionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Cotizacion.objects.all()
    serializer_class = serializer.CotizacionSerializer

class CotizacionDetalleList(generics.ListCreateAPIView):
    queryset = models.CotizacionDetalle.objects.all()
    serializer_class = serializer.CotizacionDetalleSerializer

class CotizacionDetalleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.CotizacionDetalle.objects.all()
    serializer_class = serializer.CotizacionDetalleSerializer

class HitoList(generics.ListCreateAPIView):
    queryset = models.Hito.objects.all()
    serializer_class = serializer.HitoSerializer

class HitoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Hito.objects.all()
    serializer_class = serializer.HitoSerializer

'''

class TipoProyectoList(generics.ListCreateAPIView):
    queryset = models.TipoProyecto.objects.all()
    serializer_class = serializer.TipoProyectoSerializer


class TipoProyectoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.TipoProyecto.objects.all()
    serializer_class = serializer.TipoProyectoSerializer


class EstadoList(generics.ListCreateAPIView):
    queryset = models.Estado.objects.all()
    serializer_class = serializer.EstadoSerializer

class EstadoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Estado.objects.all()
    serializer_class = serializer.EstadoSerializer


class TarifaList(generics.ListCreateAPIView):
    queryset = models.Tarifas.objects.all()
    serializer_class = serializer.TarifaSerializer

class TarifaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Tarifas.objects.all()
    serializer_class = serializer.TarifaSerializer



class HitoList(generics.ListCreateAPIView):
    queryset = models.Hitos.objects.all()
    serializer_class = serializer.HitoSerializer

class HitoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Hitos.objects.all()
    serializer_class = serializer.HitoSerializer

class CotizacionList(generics.ListCreateAPIView):
    queryset = models.Cotizaciones.objects.all()
    serializer_class = serializer.CotizacionSerializer

class CotizacionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Cotizaciones.objects.all()
    serializer_class = serializer.CotizacionSerializer
'''