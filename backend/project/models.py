from django.db import models
from django.db.models import Sum
from django.db.models.expressions import ExpressionWrapper, F
from datetime import datetime
from rest_framework import serializers

# Create your models here.

class TipoCliente(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=150)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.nombre

class Tecnologia(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=150)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.nombre

class Servicio(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=150)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.nombre

class Cliente(models.Model):
    nombre = models.CharField(max_length=150) #razon social
    razonsocial = models.CharField(max_length=200)
    ruc = models.CharField(unique=True, max_length=100)
    direccion = models.CharField(max_length=150)
    telefono = models.CharField(max_length=100)
    correo = models.EmailField(max_length=100,null=True)
    tipo = models.ForeignKey(TipoCliente, related_name = 'clientes', on_delete = models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ('fecha_creacion',)
    def __str__(self):
        return self.nombre


class Rol(models.Model):
    nombre = models.CharField(max_length = 100)
    costohora = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    descripcion = models.CharField(max_length=150, null = True)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.nombre


class Recurso(models.Model):
    nombre = models.CharField(max_length = 100)
    apellidos = models.CharField(max_length=150, null=True, blank=True)
    correo = models.EmailField(max_length=100,null=True)
    dni = models.CharField(unique=True, max_length=100, null=True, blank=True)
    telefono = models.CharField(max_length = 100, null=True, blank=True)
    direccion = models.CharField(max_length = 100, null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add = True)
    rol = models.ForeignKey(Rol, related_name = 'recursos', on_delete = models.CASCADE)
    sueldo = models.DecimalField(default=0, max_digits=10,decimal_places=2)
    horasDisponibles = models.IntegerField(default=0)
    horasDedicadas = models.IntegerField(default=0)
    class Meta:
        ordering = ('fecha_creacion',)
    def __str__(self):
        return self.nombre

class Perfil(models.Model):
    nombre = models.CharField(max_length = 100)
    descripcion = models.CharField(max_length=150, null = True)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.nombre


class Usuario(models.Model):
    nombre = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=150, null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    correo = models.EmailField(max_length=100, null=True)
    telefono = models.CharField(max_length = 100, null=True, blank=True)
    contrasena = models.CharField(max_length = 100, null=True, blank=True)
    perfil = models.ForeignKey(Perfil, related_name = 'usuarios', on_delete = models.CASCADE)
    class Meta:
        ordering = ('fecha_creacion',)
    def __str__(self):
        return self.nombre


class Proyecto(models.Model):
    nombre = models.CharField(max_length=100)
    codigo = models.CharField(max_length=50, null = True, blank = True)
    descripcion = models.CharField(max_length=150, null = True, blank = True)
    fechacreacion = models.DateTimeField(auto_now_add=True)
    fechainicio = models.DateTimeField(null=True, blank=True)
    fechafin = models.DateTimeField(null=True, blank=True)
    estado = models.CharField(max_length=50, null = True, blank = True)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    tecnologia= models.ForeignKey(Tecnologia, on_delete=models.CASCADE)
    costocotizacion = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    costoadicional = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    costototal = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    recursosasignados = models.PositiveSmallIntegerField(default=0)
    class Meta:
        ordering = ('id',)
    def __str__(self):
        return self.nombre

class Cotizacion(models.Model):
    proyecto= models.ForeignKey(Proyecto,on_delete=models.CASCADE, null = True, related_name='proyecto_cotizacion')
    fechacreacion = models.DateTimeField(auto_now_add=True)
    estado = models.CharField(max_length=20, null = True, blank = True)
    total = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    #recursos = models.ManyToManyField(CotizacionDetalle, related_name = 'recursos')

    class Meta:
        ordering = ('id',)


class CotizacionDetalle(models.Model):
    recurso_rel = models.ForeignKey(Recurso, on_delete=models.CASCADE, blank=False, null=True)
    cotizacion = models.ForeignKey(Cotizacion, on_delete=models.CASCADE, blank=False, null = True,related_name='cotizacion_detalle')
    canthoras = models.PositiveSmallIntegerField(default=0)
    subtotal = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    class Meta:
        ordering = ('id',)


class Hito(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE,  blank=False, null = True, related_name='hito_detalle')
    codigo = models.CharField(max_length=50, null = True, blank = True)
    fechainicioest = models.DateTimeField(null=True, blank=True)
    fechainicioreal = models.DateTimeField(null=True, blank=True)
    fechaentregaest = models.DateTimeField(null=True, blank=True)
    fechaentregareal = models.DateTimeField(null=True, blank=True)
    fechapagoest = models.DateTimeField(null=True, blank=True)
    fechapagoreal = models.DateTimeField(null=True, blank=True)
    porcentaje = models.DecimalField(decimal_places=3, max_digits=10, default=0)
    pago = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    estado = models.CharField(max_length=20, null = True, blank = True)
    class Meta:
        ordering = ('id',)


class Tarifario(models.Model):
    nombre = models.CharField(max_length=100)
    unidad = models.PositiveSmallIntegerField(default=0)
    class Meta:
        ordering = ('id',)


class Dedicacion(models.Model):
    nombre = models.CharField(max_length = 100)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    recurso = models.ForeignKey(Recurso, on_delete=models.CASCADE, related_name='dedicaciones')
    canthoras = models.PositiveSmallIntegerField(default=0)
    class Meta:
        ordering = ('id',)


''' 
    def save(self, *args, **kwargs):
        self.subtotal = self.recurso_rel.rol.costohora * self.canthoras
        super(CotizacionDetalle, self).save(*args, **kwargs)
'''

'''
    def save(self, *args, **kwargs):
        self.total = self.recursos.all().aggregate(Sum('subtotal'))['subtotal__sum'] if \
            self.recursos.all() else 0
        self.total = self.total if self.total else 0
        self.proyecto.save()
        super(Cotizacion, self).save(*args, **kwargs)
'''
''' 
class Estado(models.Model):
    nombre = models.CharField(max_length=100)
    class Meta:
        ordering = ('id',)


class Estimacion(models.Model):
    nombre = models.CharField(max_length = 100)
    canthoras = models.IntegerField()
    class Meta:
        ordering = ('id',)


'''