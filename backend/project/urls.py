

from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from project import views

#router = routers.DefaultRouter()
#router.register(r'tipocliente', myapp_views.TipoClienteViewset)
#router.register(r'tipoproyecto', myapp_views.TipoProyectoViewset)
#router.register(r'servicio', myapp_views.ServicioViewset)
#router.register(r'estado', myapp_views.EstadoViewset)
#router.register(r'cliente', myapp_views.ClienteViewset)
#router.register(r'proyecto', myapp_views.ProyectoViewset)

urlpatterns = [
    url(r'^cliente/$', views.ClienteList.as_view()),
    url(r'^cliente/(?P<pk>[0-9]+)/$',views.ClienteDetail.as_view()),
    url(r'^servicio/$', views.ServicioList.as_view()),
    url(r'^servicio/(?P<pk>[0-9]+)/$',views.ServicioDetail.as_view()),
    url(r'^tipocliente/$', views.TipoClienteList.as_view()),
    url(r'^tipocliente/(?P<pk>[0-9]+)/$', views.TipoClienteDetail.as_view()),
    url(r'^tecnologia/$', views.TecnologiaList.as_view()),
    url(r'^tecnologia/(?P<pk>[0-9]+)/$', views.TecnologiaDetail.as_view()),
    url(r'^rol/$', views.RolList.as_view()),
    url(r'^rol/(?P<pk>[0-9]+)/$', views.RolDetail.as_view()),
    url(r'^recurso/$', views.RecursoList.as_view()),
    url(r'^recurso/(?P<pk>[0-9]+)/$', views.RecursoDetail.as_view()),
    url(r'^perfil/$', views.PerfilList.as_view()),
    url(r'^perfil/(?P<pk>[0-9]+)/$', views.PerfilDetail.as_view()),
    url(r'^usuario/$', views.UsuarioList.as_view()),
    url(r'^usuario/(?P<pk>[0-9]+)/$', views.UsuarioDetail.as_view()),
    url(r'^proyecto/$', views.ProyectoList.as_view()),
    url(r'^proyecto/(?P<pk>[0-9]+)/$', views.ProyectoDetail.as_view()),
    url(r'^cotizacion/$', views.CotizacionList.as_view()),
    url(r'^cotizacion/(?P<pk>[0-9]+)/$', views.CotizacionDetail.as_view()),
    url(r'^cotizaciondetalle/$', views.CotizacionDetalleList.as_view()),
    url(r'^cotizaciondetalle/(?P<pk>[0-9]+)/$', views.CotizacionDetalleDetail.as_view()),
    url(r'^hito/$', views.HitoList.as_view()),
    url(r'^hito/(?P<pk>[0-9]+)/$', views.HitoDetail.as_view())

]
urlpatterns = format_suffix_patterns(urlpatterns)