# Generated by Django 2.2.5 on 2019-10-04 21:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0037_auto_20191004_1559'),
    ]

    operations = [
        migrations.RenameField(
            model_name='rol',
            old_name='costohora',
            new_name='costoxhora',
        ),
    ]
