# Generated by Django 2.2.5 on 2019-10-11 22:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0054_auto_20191011_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cotizacion',
            name='recursos',
            field=models.ManyToManyField(blank=True, related_name='recursos', to='project.CotizacionDetalle'),
        ),
    ]
