# Generated by Django 2.2.5 on 2019-10-04 21:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0043_auto_20191004_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='recurso',
            name='sueldo',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='rol',
            name='costohora',
            field=models.FloatField(default=0),
        ),
    ]
