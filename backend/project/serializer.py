from rest_framework import serializers, fields
from . import models


class TipoClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TipoCliente
        fields = '__all__'

class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cliente
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['tipo'] = TipoClienteSerializer(instance.tipo).data
        return response

class ServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Servicio
        fields = '__all__'


class TecnologiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tecnologia
        fields = '__all__'

class RecursoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Recurso
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['rol'] = RolSerializer(instance.rol).data
        return response

class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Rol
        fields = '__all__'


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Usuario
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['perfil'] = PerfilSerializer(instance.perfil).data
        return response

class PerfilSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Perfil
        fields = '__all__'


class HitoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Hito
        fields='__all__'


class ProyectoSerializer(serializers.ModelSerializer):
    hito_detalle = serializers.SerializerMethodField()
    class Meta:
        model = models.Proyecto
        fields = ('id', 'nombre', 'codigo','descripcion', 'fechacreacion', 'fechainicio','fechafin',
                  'estado', 'cliente', 'tecnologia', 'servicio', 'costototal', 'costocotizacion',
                  'costoadicional','recursosasignados','hito_detalle')

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['servicio'] = ServicioSerializer(instance.servicio).data
        response['tecnologia'] = TecnologiaSerializer(instance.tecnologia).data
        response['cliente'] = ClienteSerializer(instance.cliente).data
        return response

    def get_hito_detalle(self, instance):
        milestones = instance.hito_detalle.all().order_by('id')
        return HitoSerializer(milestones, many=True).data

class CotizacionDetalleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CotizacionDetalle
        fields = '__all__'

class CotizacionSerializer(serializers.ModelSerializer):
    cotizacion_detalle = serializers.SerializerMethodField()
    class Meta:
        model = models.Cotizacion
        fields = ('id','proyecto','fechacreacion','estado','total','cotizacion_detalle')

    def get_cotizacion_detalle(self, instance):
        detalles = instance.cotizacion_detalle.all().order_by('id')
        return CotizacionDetalleSerializer(detalles, many=True).data


'''
class HitoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Hitos
        fields = '__all__'



class EstadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Estado
        fields = '__all__'


class TarifaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Tarifas
        fields = '__all__'
'''